const { Schema, model } = require("mongoose")

const DocumentModel = new Schema({
  _id: { type: String, required: false },
  room_id: { type: String, required: false },
  name: { type: String, required: false },
  version: { type: Number, required: false },
  isBigEndian: { type: Boolean, required: false },
  useIndices: { type: Boolean, required: false },
  useStyles: { type: Boolean, required: false },
  useAttributes: { type: Boolean, required: false },
  useAttributeStrings: { type: Boolean, required: false },
  bytesPerAttribute: { type: Number, required: false },
  hashSlotCount: { type: Number, required: false },
  atO1Numbers: { type: Object, required: false },
  encoding: { type: String, required: false },
  data: { type: Object, required: false },
  columns: { type: Object, required: false }
})

module.exports = model("Document", DocumentModel)