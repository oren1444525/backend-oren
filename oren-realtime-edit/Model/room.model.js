const { Schema, model } = require("mongoose")

const Room = new Schema({
    _id: String,
    name: String,
    creator: String,
    invitation_code: String,
    main_table: String,
    usesMSBP: Boolean
})

module.exports = model("Rooms", Room);