const Rooms = require("../Model/room.model")
const Room_User = require("../Model/room-user.model")
const Document = require("../Model/document.model")
const User = require("../Model/user.model")
const uuid = require("uuid")

exports.get_all_users = (req, res) => {
    Room_User.find({room_id: req.query.roomId})
        .then(users => {
            return Promise.all(users.map((item) => {
                return User.findOne({_id: item.user_id})
                    .then(user => ({
                        _id: user._id,
                        username: user.username,
                        email: user.email
                    }))
            }))
        })
        .then(answer => {
            res.status(200).json({
                users: answer
            })
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
}

exports.get_all_tables = (req, res) => {
    Document.find({room_id: req.query.roomId})
        .then(tables => {
            const answer = tables.map((item) => ({
                id: item._id,
                name: item.name,
                room_id: item.room_id,
                version: item.version,
                isBigEndian: item.isBigEndian,
                useIndices: item.useIndices,
                useStyles: item.useStyles,
                useAttributes: item.useAttributes,
                useAttributeStrings: item.useAttributeStrings,
                bytesPerAttribute: item.bytesPerAttribute,
                hashSlotCount: item.hashSlotCount,
                atO1Numbers: item.atO1Numbers,
                encoding: item.encoding,
                data: item.data,
                columns: item.columns
            }))

            res.status(200).json({
                tables: answer
            })
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
}

exports.get_all_rooms = (req, res) => {
    Rooms.find({})
        .then(rooms => {
            res.status(200).json({
                rooms: rooms
            })
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
}

exports.get_room = (req, res) => {
    Rooms.findOne({_id: req.query.roomId})
        .then(room => {
            if (!room) {
                res.status(409).json({
                    message: `Room not found`
                })
            }
            else {
                res.status(200).json({
                    id: room._id,
                    name: room.name,
                    creator: room.creator,
                    invitation_code: room.invitation_code,
                    main_table: room.main_table,
                    usesMSBP: room.usesMSBP
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
}

exports.get_users_rooms = (req, res) => {
    Room_User.find({user_id: req.userData.userId})
        .then(rooms => {
            if (rooms.length === 0) {
                res.status(200).json({
                    my_rooms: []
                })
                return
            }

            Rooms.find({ $or: rooms.map(item => ({_id: item.room_id})) })
                .then(my_rooms => {
                    res.status(200).json({
                        my_rooms: my_rooms
                    })
                })
                .catch(err => {
                    console.log(err)
                    res.status(500).json({
                        error: err
                    })
                })
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
}

exports.add_user = (req, res) => {
    User.findOne({email: req.body.email})
        .then(user => {
            if (!user) {
                res.status(409).json({
                    message: `User not found`
                });
            }
            else {
                Rooms.findOne({_id: req.query.roomId})
                    .then(room => {
                        if (!room) {
                            res.status(404).json({
                                error: "Room not found"
                            })
                        }
                        else if (room.creator !== req.userData.userId) {
                            res.status(400).json({
                                error: "You aren't an admin"
                            })
                        }
                        else {
                            Room_User.findOne({ room_id: req.query.roomId, user_id: user._id })
                                .then(room_user => {
                                    if (room_user) {
                                        res.status(400).json({
                                            error: "User already added"
                                        })
                                    }
                                    else {
                                        Room_User.create({room_id: req.query.roomId, user_id: user._id})
                                            .then(() => {
                                                res.status(200).json({
                                                    message: "ok"
                                                })
                                            })
                                            .catch(err => {
                                                console.log(err)
                                                res.status(500).json({
                                                    error: err
                                                })
                                            })
                                    }
                                })
                        }
                    })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
}

exports.add_room = (req, res) => {
    const room_id = uuid.v4()
    const table_id = uuid.v4()
    Rooms.create({_id: room_id,
        name: req.body.name,
        creator: req.userData.userId,
        invitation_code: uuid.v4(),
        main_table: table_id,
        usesMSBP: req.body.usesMSBP})
        .then(() => {
            Document.create({
                _id: table_id,
                room_id: room_id,
                name: req.body.tableData.fileName,
                version: req.body.tableData.version,
                isBigEndian: req.body.tableData.isBigEndian,
                useIndices: req.body.tableData.useIndices,
                useStyles: req.body.tableData.useStyles,
                useAttributes: req.body.tableData.useAttributes,
                useAttributeStrings: req.body.tableData.useAttributeStrings,
                bytesPerAttribute: req.body.tableData.bytesPerAttribute,
                hashSlotCount: req.body.tableData.hashSlotCount,
                atO1Numbers: req.body.tableData.atO1Numbers,
                encoding: req.body.tableData.encoding,
                data: req.body.tableData.data,
                columns: req.body.tableData.columns
            })
                .then(() => {
                    Room_User.create({room_id: room_id, user_id: req.userData.userId})
                        .then(
                            res.status(200).json({
                                roomId: room_id,
                                tableId: table_id
                            })
                        )
                        .catch(err => {
                            console.log(err)
                            res.status(500).json({
                                error: err
                            })
                        })
                })
                .catch(err => {
                    console.log(err)
                    res.status(500).json({
                        error: err
                    })
                })
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
}

exports.rename_room = (req, res) => {
    Rooms.findOne({_id: req.query.roomId})
        .then(room => {
            if (!room) {
                throw new Error('Room not found');
            }
            else if (room.creator !== req.userData.userId) {
                throw new Error("You aren't an admin");
            }

            room.name = req.body.name
            return room.save()
        })
        .then(() => {
            res.status(200).json({
                message: `Room updated successfully`
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err.message
            });
        });
}

exports.room_delete = (req, res) => {
    Rooms.findOne({_id: req.query.roomId})
        .then( room => {
            if (room.creator !== req.userData.userId) {
                res.status(400).json({
                    error: "You aren't an admin"
                })
            }
            else if (room) {
                Rooms.deleteOne({ _id: room._id })
                    .then(() => {
                        Document.deleteMany({room_id: room._id})
                            .then(() => {
                                res.status(200).json({
                                    message: `User deleted successfully!`
                                })
                            }).catch(err => {
                            res.status(500).json({
                                error: err
                            })
                        })
                    }).catch(err => {
                    res.status(500).json({
                        error: err
                    })
                })
            }
            else {
                res.status(404).json({
                    error: "Room not found"
                })
            }

        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        });
}

exports.delete_room_users = (req, res) => {
    Room_User.findOne({room_id: req.query.roomId, user_id: req.query.user_id})
        .then(room_user => {
            if (!room_user) {
                res.status(404).json({
                    error: "User not found"
                })
            }
            else {
                Rooms.findOne({_id: room_user.room_id})
                    .then(room => {
                        if (room.creator !== req.userData.userId && req.userData.userId === room_user.user_id) {
                            Room_User.deleteOne({ _id: room_user._id })
                                .then(() => {
                                    res.status(200).json({
                                        message: `User deleted successfully!`
                                    })
                                }).catch(err => {
                                res.status(500).json({
                                    error: err
                                })
                            })
                        }
                        else if (room.creator !== req.userData.userId) {
                            res.status(400).json({
                                error: "You aren't an admin"
                            })
                        }
                        else if (req.userData.userId === room_user.user_id) {
                            res.status(400).json({
                                error: "You can't delete yourself"
                            })
                        }
                        else {
                            Room_User.deleteOne({ _id: room_user._id })
                                .then(() => {
                                    res.status(200).json({
                                        message: `User deleted successfully!`
                                    })
                                }).catch(err => {
                                res.status(500).json({
                                    error: err
                                })
                            })
                        }
                    })
                    .catch(err => {
                        res.status(500).json({
                            error: err
                        })
                    })
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

exports.invite_user = (req, res) => {
    Rooms.findOne({invitation_code: req.body.invitation_code})
        .then(room => {
            if (!room) {
                res.status(404).json({
                    error: "Room not found"
                })
            }
            else {
                Room_User.findOne({ room_id: room._id, user_id: req.userData.userId })
                    .then(room_user => {
                        if (room_user) {
                            res.status(400).json({
                                error: "User already added"
                            })
                        }
                        else {
                            Room_User.create({room_id: room._id, user_id: req.userData.userId})
                                .then(() => {
                                    res.status(200).json({
                                        roomId: room._id,
                                        tableId: room.main_table
                                    })
                                })
                                .catch(err => {
                                    res.status(500).json({
                                        error: err
                                    })
                                })
                        }
                    })
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}