const mongoose = require("mongoose")
const Document = require("./Model/document.model")
const jwtAuth = require('socketio-jwt-auth');
const User = require("./Model/user.model");
const axios = require("axios")
require("dotenv").config();

const dbHost = process.env.DB_HOST || 'mongodb';
const dbPort = process.env.DB_PORT || '27017';
const dbName = process.env.DB_NAME || 'bezkoder_db';
const dbUser = process.env.DB_USER || 'root';
const dbPassword = process.env.DB_PASSWORD || '123456';

const mongoUri = `mongodb://${dbUser}:${dbPassword}@${dbHost}:${dbPort}/${dbName}?authSource=admin`;

mongoose.connect(mongoUri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
}).then(() => {
  console.log('Connected to MongoDB successfully');
}).catch((error) => {
  console.error('Connection error:', error);
});

const io = require("socket.io")(3001, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
})

io.use(jwtAuth.authenticate({
  secret: 'secret',    // required, used to verify the token's signature
  algorithm: 'HS256'        // optional, default to be HS256
}, function(payload, done) {
  // done is a callback, you can use it as follows
  User.findOne({id: payload.sub}, function(err, user) {
    if (err) {
      // return error
      return done(err);
    }
    if (!user) {
      // return fail with an error message
      return done(null, false, 'user does not exist');
    }
    // return success with a user info
    return done(null, user);
  });
}));

io.on("connection", socket => {
  socket.on("get-document", async (tableId, roomId) => {
    const table = await findDocumentOrNull(tableId, roomId)

    if (table == null) {
      socket.emit("load-document", false)
    } else {
      socket.join(tableId)
      socket.emit("load-document", true, table.data, table.columns, table.name)
    }

    socket.on("click-mouse", (row, col, userId) => {
      socket.broadcast.to(tableId).emit("set-color", row, col, userId)
    })

    socket.on("no-click-mouse", (userId) => {
      socket.broadcast.to(tableId).emit("delete-color", userId)
    })

    socket.on("send-changes", delta => {
      socket.broadcast.to(tableId).emit("receive-changes", delta)
    })

    socket.on("send-cols", async (cols, title, translationCol, langFrom, langTo) => {
      const tables = await findAllDocumentInRoom(roomId, cols, title, translationCol, langFrom, langTo);

      tables.forEach((item) => {
        io.sockets.in(item._id).emit("receive-cols", cols, item.data);
      });
    });

    socket.on("send-rows", (rows) => {
      socket.broadcast.to(tableId).emit("receive-rows", rows)
    })

    socket.on("send-table-name", async (name) => {
      socket.broadcast.to(tableId).emit("receive-table-name", name)
    })

    socket.on("send-tables", async (Id) => {
      const tables = await findAllDocumentInRoom(roomId);

      tables.forEach((item) => {
        socket.broadcast.to(item._id).emit("receive-tables")
      });

      if (tableId !== undefined) socket.broadcast.to(Id).emit("receive-tables", tableId)
    })

    socket.on("send-room-name", async (name) => {
      const tables = await findAllDocumentInRoom(roomId);

      tables.forEach((item) => {
        socket.broadcast.to(item._id).emit("receive-room-name", name)
      });
    })

    socket.on("send-users", async () => {
      const tables = await findAllDocumentInRoom(roomId);

      tables.forEach((item) => {
        socket.broadcast.to(item._id).emit("receive-users")
      });
    })

    socket.on("save-document", async (data, columns) => {
      await Document.findByIdAndUpdate(tableId, { data, columns })
    })
  })
})

async function findDocumentOrNull(tableId, roomId) {
  if (tableId == null || roomId == null) return

  const document = await Document.findOne({_id: tableId, room_id: roomId})
  if (document) return document
  return null
}

async function findAllDocumentInRoom(roomId, cols, title, translationCol, langFrom, langTo) {
  if (roomId == null) return

  let tables = await Document.find({room_id: roomId})
  let response = null

  if (cols != null) {
    for (const table of tables) {
      if (translationCol != null && tables.length < 20) {
        const textForTranslate = table.data.map((item) => {
          return item[translationCol]
        })

        if (textForTranslate.join("").length >= 10000) {
          response = null
        } else {
          response = await axios.post(`https://translate.api.cloud.yandex.net/translate/v2/translate`,
              {sourceLanguageCode: langFrom, folderId: "b1gbi9p05hufm79d5rlo", texts: textForTranslate, targetLanguageCode: langTo}, {
                headers: {
                  "Authorization": "Bearer t1.9euelZrHmZiOx8yZm8iVy8vKy8iake3rnpWax56dm4zJy8jHmIvGy87Ki5vl8_caXgxM-e87Rjse_t3z91oMCkz57ztGOx7-zef1656Vmo2bjpTNyZeRzIzGnpGPl4yZ7_zF656Vmo2bjpTNyZeRzIzGnpGPl4yZ.o6Kfs9aPudmU7dWF0t8odxIywMqHMbI17WKWYhEoClOutzsxQ5EEiTvR7AdGk5stGaY9jkXcg12uLgwKqTTxBw"
                }
              })
        }
      }

      const data = table.data.map((item, index) => {
        const arr = objectToArray(item, cols.length - 1)

        if (table.columns > cols) {
          arr.splice(arr.length - 2, 1)
        }
        else {
          const items = index === 0 ? title : response != null ? response.data.translations[index]["text"] : ""
          arr.splice(arr.length - 1, 0, items)
        }

        return Object.fromEntries(arr.map((value, index) => [index, value]))
      })

      await Document.updateOne({ _id: table._id }, { $set: { columns: cols, data: data } })
      tables = await Document.find({room_id: roomId})
    }
  }

  return tables
}

function objectToArray(obj, length) {
  const arr = new Array(length).fill('');
  for (const key in obj) {
    arr[parseInt(key)] = obj[key];
  }
  return arr;
}

const PORT = process.env.NODE_DOCKER_PORT  || 5000;

const app = require("./app");

app.listen(PORT , () => {
  console.log("Running on port :" + PORT);
})